<?php declare(strict_types=1);

namespace App;

use Predis\Client;

class Cache
{
    protected $client;
    
    public function __construct()
    {
        $this->client = new Client();
    }
    
    public function savePerson(array $person) : void
    {
        $this->client->rpush('people', serialize($person));
        $this->client->expire('people', 60);
    }
    
    public function getPeople() : array
    {
        $serializedPeople = $this->client->lrange('people', 0, -1);
        
        $people = array_map(function ($serializedPerson) {
            return unserialize($serializedPerson);
        }, $serializedPeople);
        
        return $people;
    }
}
