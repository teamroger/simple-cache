<?php declare(strict_types=1);

ini_set('display_errors', 'On');
error_reporting(E_ALL);

require_once __DIR__ . '/vendor/autoload.php';

use App\Cache;

$cache = new Cache();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $cache->savePerson([
        'name' => $_POST['name'],
        'hair' => $_POST['hair'],
        'eyes' => $_POST['eyes'],
    ]);
}

$people = $cache->getPeople();

require_once __DIR__ . '/template.html.php';
