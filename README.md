# Simple Cache

##Playing with Redis cache

### Installing Redis
$ sudo apt-get install redis-server

### Test Redis is working
$ redis-cli
redis> ping

Shoud return `PONG`

### Installing the "predis" PHP Client for Talking to a Redis Server
$ composer require predis/predis
