<!doctype html>

<html lang="end">
    <head>
        <meta charset="utf-8" />
        
        <title>Simple Cache</title>
    </head>
    
    <body>
        <h1>Simple Cache</h1>
        
        <p>Create records and cache them.</p>
        
        <p>All records get deleted 1 minute after the last one was created.</p>
        
        <form method="post" action="/">
            <p>
                <label for="name">Name</label>
                <input type="text" id="name" name="name" />
            </p>
            
            <p>
                <label for="hair">Hair colour</label>
                <input type="text" id="hair" name="hair" />
            </p>
            
            <p>
                <label for="eyes">Eye colour</label>
                <input type="text" id="eyes" name="eyes" />
            </p>
            
            <p>
                <input type="submit" value="Cache!" />
            </p>
        </form>
        
        <?php foreach($people as $person): ?>
        <div>
            <h4>Person</h4>
            <ul>
                <li>Name: <?php echo $person['name'] ?></li>
                <li>Hair: <?php echo $person['hair'] ?></li>
                <li>Eyes: <?php echo $person['eyes'] ?></li>
            </ul>
        </div>
        <?php endforeach ?>
    </body>
</html>
